<?php

namespace UnicaenAuthentification;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Point d'entrée du module d'authentification Unicaen.
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface
{
    /**
     * @return array
     * @see ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     * @see AutoloaderProviderInterface
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    /**
     * This method is called once the MVC bootstrapping is complete,
     * after the "loadModule.post" event, once $application->bootstrap() is called.
     *
     * @param EventInterface $e
     *
     * @see BootstrapListenerInterface
     */
    public function onBootstrap(EventInterface $e)
    {

    }

    /**
     * @return array
     * @see ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return [
            //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
            'aliases' => array(
                'zfcuser_doctrine_em' => 'Doctrine\ORM\EntityManager',
            ),
            //===========================================


            'factories' => [
                //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
                'zfcuser_module_options' => function ($sm) {
                    $config = $sm->get('Configuration');
                    return new Options\ModuleOptions(isset($config['zfcuser']) ? $config['zfcuser'] : array());
                },
                //===========================================

            ],
        ];
    }
}