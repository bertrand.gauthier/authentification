<?php

namespace UnicaenAuthentification\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenAuthentification\Service\UserContext;
use Zend\View\Helper\Url;

class UserUsurpationHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return UserUsurpationHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var Url $urlHelper */
        $urlHelper = $container->get('ViewHelperManager')->get('url');
        $url = $urlHelper->__invoke('utilisateur/default', ['action' => 'usurper-identite']);

        /** @var UserContext $userContextService */
        $userContextService = $container->get('AuthUserContext');

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        $usurpationAllowed = in_array(
            $userContextService->getIdentityUsername(),
            $moduleOptions->getUsurpationAllowedUsernames());
        $usurpationEnCours = $userContextService->isUsurpationEnCours();

        $helper = new UserUsurpationHelper($userContextService);
        $helper->setUrl($url);
        $helper->setUsurpationEnabled($usurpationAllowed);
        $helper->setUsurpationEnCours($usurpationEnCours);

        return $helper;
    }
}