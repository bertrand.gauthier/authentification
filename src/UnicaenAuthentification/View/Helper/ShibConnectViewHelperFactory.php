<?php

namespace UnicaenAuthentification\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;

class ShibConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return ShibConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getShib();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $title = $config['title'] ?? null;
        $description = $config['description'] ?? null;

        $helper = new ShibConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setTitle($title ?? ShibConnectViewHelper::TITLE);
        $helper->setDescription($description);

        return $helper;
    }
}