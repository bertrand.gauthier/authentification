<?php

namespace UnicaenAuthentification\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;

/**
 * Class LocalConnectViewHelperFactory
 */
class LocalConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return LocalConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getLocal();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $title = $config['title'] ?? null;
        $description = $config['description'] ?? null;

        $helper = new LocalConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setTitle($title ?? LocalConnectViewHelper::TITLE);
        $helper->setDescription($description);
        $helper->setPasswordReset(true);

        return $helper;
    }
}