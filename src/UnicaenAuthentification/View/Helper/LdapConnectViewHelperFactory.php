<?php

namespace UnicaenAuthentification\View\Helper;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;

class LdapConnectViewHelperFactory
{
    /**
     * @param ContainerInterface $container
     * @return LdapConnectViewHelper
     */
    public function __invoke(ContainerInterface $container)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');
        $config = $moduleOptions->getLdap();

        $enabled = isset($config['enabled']) && (bool) $config['enabled'];
        $description = $config['description'] ?? null;

        $helper = new LdapConnectViewHelper();
        $helper->setEnabled($enabled);
        $helper->setDescription($description);

        return $helper;
    }
}