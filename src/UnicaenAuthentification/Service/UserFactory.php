<?php

namespace UnicaenAuthentification\Service;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Options\ModuleOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcUser\Options\ModuleOptions as ZfcUserModuleOptions;

class UserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /** @var ZfcUserModuleOptions $zfcUserModulesOptions */
        $zfcUserModulesOptions = $container->get('zfcuser_module_options');

        /** @var UserMapper $mapper */
        $mapper = $container->get('zfcuser_user_mapper');

        $service = new User();
        $service->setUserMapper($mapper);
        $service->setOptions($moduleOptions);
        $service->setZfcUserOptions($zfcUserModulesOptions);

        return $service;
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }
}