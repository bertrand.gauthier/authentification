<?php

namespace UnicaenAuthentification\Service;

use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenApp\ServiceManager\ServiceLocatorAwareTrait;
use UnicaenAuthentification\Options\ModuleOptions;
use UnicaenApp\ServiceManager\ServiceLocatorAwareInterface;

abstract class AbstractService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;
    use EntityManagerAwareTrait;

    /**
     * @var \BjyAuthorize\Service\Authorize
     */
    private $serviceAuthorize;

    /**
     * @return \UnicaenAuthentification\Service\AuthorizeService
     */
    protected function getServiceAuthorize()
    {
        if (!$this->serviceAuthorize) {
            $this->serviceAuthorize = $this->getServiceLocator()->get('BjyAuthorize\Service\Authorize');
        }

        return $this->serviceAuthorize;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if (!$this->entityManager) {
            $moduleOptions = $this->getServiceLocator()->get('unicaen-auth_module_options');
            /* @var $moduleOptions ModuleOptions */
            $this->entityManager = $this->getServiceLocator()->get($moduleOptions->getEntityManagerName());
        }

        return $this->entityManager;
    }
}