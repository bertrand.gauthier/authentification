<?php

namespace UnicaenAuthentification\Service;

use Doctrine\ORM\EntityManagerInterface;
use UnicaenUtilisateur\Entity\Db\AbstractUser;
use UnicaenAuthentification\Options\ModuleOptions;
use ZfcUser\Entity\UserInterface as UserEntityInterface;
use ZfcUser\Mapper\UserInterface;

class UserMapper implements UserInterface
{
    //========== repris du module zf-commons/zfc-user-doctrine-orm abandonné =========
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;

    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * UserMapper constructor.
     *
     * @param EntityManagerInterface $em
     * @param ModuleOptions          $options
     */
    public function __construct(EntityManagerInterface $em, ModuleOptions $options)
    {
        $this->em      = $em;
        $this->options = $options;
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail($email)
    {
        $er = $this->em->getRepository($this->options->getUserEntityClass());

        return $er->findOneBy(array('email' => $email));
    }

    /**
     * {@inheritdoc}
     */
    public function findByUsername($username)
    {
        $er = $this->em->getRepository($this->options->getUserEntityClass());

        return $er->findOneBy(array('username' => $username));
    }

    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        $er = $this->em->getRepository($this->options->getUserEntityClass());

        return $er->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function insert(UserEntityInterface $entity)
    {
        return $this->persist($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function update(UserEntityInterface $entity)
    {
        return $this->persist($entity);
    }

    /**
     * @param UserEntityInterface $entity
     * @return mixed
     */
    protected function persist(UserEntityInterface $entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
    //===================



    /**
     * Recherche un utilisateur par son username (identifiant de connexion).
     *
     * @param string $username
     * @return AbstractUser|null
     */
    public function findOneByUsername($username)
    {
        /** @var AbstractUser $user */
        $user = $this->em->getRepository($this->options->getUserEntityClass())->findOneBy(['username' => $username]);

        return $user;
    }

    /**
     * @param string $token
     * @return AbstractUser
     */
    public function findOneByPasswordResetToken($token)
    {
        /** @var AbstractUser $user */
        $user = $this->em->getRepository($this->options->getUserEntityClass())->findOneBy(['passwordResetToken' => $token]);

        return $user;
    }
}