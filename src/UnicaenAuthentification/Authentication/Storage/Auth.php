<?php

namespace UnicaenAuthentification\Authentication\Storage;

use UnicaenAuthentification\Authentication\SessionIdentity;

/**
 * Storage chargé de stocker/restituer des infos concernant l'authentification effectuée (type, etc.)
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class Auth extends AbstractStorage
{
    const TYPE = 'auth';

    const KEY_TYPE = 'type';

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @param array $identityArray
     * @return string|null
     */
    public static function extractTypeFromIdentityArray(array $identityArray): ?string
    {
        return $identityArray[self::TYPE][self::KEY_TYPE] ?? null;
    }

    /**
     * @inheritDoc
     */
    protected function canHandleContents(SessionIdentity $sessionIdentity): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function findIdentity(): array
    {
        /** @var SessionIdentity $sessionIdentity */
        $sessionIdentity = $this->storage->read();

        return [
            self::KEY_TYPE => $sessionIdentity->getType(),
        ];
    }
}