<?php

namespace UnicaenAuthentification\Authentication\Storage;

use Interop\Container\ContainerInterface;
use UnicaenAuthentification\Authentication\Adapter\Db as DbAdapter;
use UnicaenAuthentification\Options\ModuleOptions;
use Zend\Authentication\Storage\Session;
use Zend\Session\SessionManager;
use ZfcUser\Mapper\UserInterface as UserMapper;

class DbFactory
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $moduleOptions
     * @return Db
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $moduleOptions = null)
    {
        /** @var UserMapper $mapper */
        $mapper = $container->get('zfcuser_user_mapper');

        /** @var ModuleOptions $moduleOptions */
        $moduleOptions = $container->get('unicaen-auth_module_options');

        /** @var SessionManager $sessionManager */
        $sessionManager = $container->get(SessionManager::class);

        $storage = new Db();
        $storage->setStorage(new Session(DbAdapter::class, null, $sessionManager));
        $storage->setMapper($mapper);
        $storage->setModuleOptions($moduleOptions);

        return $storage;
    }
}