<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use UnicaenAuthentification\Options\Traits\ModuleOptionsAwareTrait;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\EventManager\EventInterface;
use Zend\Session\Container as SessionContainer;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;
use ZfcUser\Entity\UserInterface;
use ZfcUser\Mapper\UserInterface as UserMapperInterface;

/**
 * Classe abstraite des adpater d'authentification à partir de la base de données.
 * 
 * Ajout par rapport à la classe mère : si aucune base de données ou table n'existe,
 * l'authentification ne plante pas (i.e. renvoit false).
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
abstract class AbstractDb extends AbstractAdapter
{
    use ModuleOptionsAwareTrait;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var AdapterChainEvent
     */
    protected $event;

    /**
     * @var UserMapperInterface
     */
    protected $mapper;

    /**
     * @inheritDoc
     */
//    public function authenticate(EventInterface $e): bool
    public function authenticate( $e): bool
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        $this->event = $e->getTarget();

        if ($this->event->getIdentity()) {
            return true;
        }

        if ($this->isSatisfied()) {
            $storage = $this->getStorage()->read();
            $this->event
                ->setIdentity($storage['identity'])
                ->setCode(AuthenticationResult::SUCCESS)
                ->setMessages(array('Authentication successful.'));
            return true;
        }

        $userObject = $this->fetchUserObject();
        if ($userObject === null) {
            return false;
        }

        if ($this->moduleOptions->getEnableUserState()) {
            // Don't allow user to login if state is not in allowed list
            if (!in_array($userObject->getState(), $this->moduleOptions->getAllowedLoginStates())) {
                $this->event
                    ->setCode(AuthenticationResult::FAILURE_UNCATEGORIZED)
                    ->setMessages(["Ce compte utilisateur a été désactivé"]);
                $this->setSatisfied(false);
                return false;
            }
        }

        $result = $this->authenticateUserObject($userObject);
        if ($result === false) {
            return false;
        }

        // regen the id
        $session = new SessionContainer($this->getStorage()->getNamespace());
        $session->getManager()->regenerateId();

        // Success!
        $identity = $this->createSessionIdentity($userObject->getUsername());
        $this->event->setIdentity($identity);
        $this->setSatisfied(true);
        $storage = $this->getStorage()->read();
        $storage['identity'] = $this->event->getIdentity();
        $this->getStorage()->write($storage);
        $this->event
            ->setCode(AuthenticationResult::SUCCESS)
            ->setMessages(array('Authentication successful.'));

        return true;
    }

    /**
     * @return \ZfcUser\Entity\UserInterface|null
     */
    abstract protected function fetchUserObject(): ?UserInterface;

    /**
     * @param \ZfcUser\Entity\UserInterface $userObject
     * @return bool
     */
    abstract protected function authenticateUserObject(UserInterface $userObject): bool;

    /**
     * setMapper
     *
     * @param UserMapperInterface $mapper
     * @return self
     */
    public function setMapper(UserMapperInterface $mapper): self
    {
        $this->mapper = $mapper;

        return $this;
    }
}