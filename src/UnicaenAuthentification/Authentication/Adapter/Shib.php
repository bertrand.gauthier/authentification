<?php

namespace UnicaenAuthentification\Authentication\Adapter;

use UnicaenAuthentification\Controller\AuthController;
use UnicaenAuthentification\Options\Traits\ModuleOptionsAwareTrait;
use UnicaenAuthentification\Service\Traits\ShibServiceAwareTrait;
use UnicaenAuthentification\Service\User;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\EventManager\EventInterface;
use Zend\Http\Response;
use Zend\Router\RouteInterface;
use ZfcUser\Authentication\Adapter\AdapterChainEvent;

/**
 * CAS authentication adpater
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class Shib extends AbstractAdapter
{
    use ModuleOptionsAwareTrait;
    use ShibServiceAwareTrait;

    const TYPE = 'shib';

    /**
     * @var string
     */
    protected $type = self::TYPE;

    /**
     * @var AuthenticationService
     */
    protected $authenticationService;

    /**
     * @param AuthenticationService $authenticationService
     * @return self
     */
    public function setAuthenticationService(AuthenticationService $authenticationService): self
    {
        $this->authenticationService = $authenticationService;
        return $this;
    }

    /**
     * @var RouteInterface
     */
    private $router;

    /**
     * @param RouteInterface $router
     */
    public function setRouter(RouteInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @var User
     */
    private $userService;

    /**
     * @param User $userService
     */
    public function setUserService(User $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @inheritDoc
     */
//    public function authenticate(EventInterface $e)
    public function authenticate( $e)
    {
        // NB: Dans la version 3.0.0 de zf-commons/zfc-user, cette méthode prend un EventInterface.
        // Mais dans la branche 3.x, c'est un AdapterChainEvent !
        // Si un jour c'est un AdapterChainEvent qui est attendu, plus besoin de faire $e->getTarget().
        $event = $e->getTarget(); /** @var AdapterChainEvent $event */

        $type = $event->getRequest()->getPost()->get('type');
        if ($type !== $this->type) {
            return false;
        }

        /* DS : modification liée à une boucle infinie lors de l'authentification CAS */
        if ($this->isSatisfied()) {
            $storage = $this->getStorage()->read();
            $event
                ->setIdentity($storage['identity'])
                ->setCode(AuthenticationResult::SUCCESS)
                ->setMessages(['Authentication successful.']);
            return true;
        }

        $shibUser = $this->shibService->getAuthenticatedUser();

        if ($shibUser === null) {
            $redirectUrl = $this->router->assemble(['type' => 'shib'], [
                    'name' => 'zfcuser/authenticate',
                    'query' => ['redirect' => $event->getRequest()->getQuery()->get('redirect')]]
            );
            $shibbolethTriggerUrl = $this->router->assemble([], [
                    'name' => 'auth/shibboleth',
                    'query' => ['redirect' => $redirectUrl]]
            );
            $response = new Response();
            $response->getHeaders()->addHeaderLine('Location', $shibbolethTriggerUrl);
            $response->setStatusCode(302);

            return $response;
        }

        $identity = $this->createSessionIdentity($shibUser->getEppn());

        $event->setIdentity($identity);
        $this->setSatisfied(true);
        $storage = $this->getStorage()->read();
        $storage['identity'] = $event->getIdentity();
        $this->getStorage()->write($storage);
        $event
            ->setCode(AuthenticationResult::SUCCESS)
            ->setMessages(['Authentication successful.']);

        /* @var $userService User */
        $this->userService->userAuthenticated($shibUser);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function logout(EventInterface $e)
    {
        $storage = $this->getStorage()->read();
        if (! isset($storage['identity'])) {
            return;
        }

        parent::logout($e);

        // désactivation de l'usurpation d'identité éventuelle
        $this->shibService->deactivateUsurpation();

        // URL vers laquelle on redirige après déconnexion
        $returnUrl = $this->router->assemble([], [
                'name' => 'zfcuser/logout',
                'force_canonical' => true,
        ]);
        $shibbolethLogoutUrl = $this->shibService->getLogoutUrl($returnUrl);

        $response = new Response();
        $response->getHeaders()->addHeaderLine('Location', $shibbolethLogoutUrl);
        $response->setStatusCode(302);

        /**
         * Problème : l'IDP Shibboleth ne redirige pas correctement vers l'URL demandée après déconnexion.
         * Solution : pour l'instant, on fait le nécessaire ici (qui devrait être fait normalement dans
         * {@see AuthController::logoutAction()} si l'IDP redonnait la main à l'appli.
         * todo: supprimer cette verrue lorsque l'IDP redirigera correctement.
         */
        $this->authenticationService->clearIdentity();
    }
}