<?php

namespace UnicaenAuthentification\Form;

use ZfcUser\Options\AuthenticationOptionsInterface;

class ShibLoginForm extends LoginForm
{
    /**
     * @var string[]
     */
    protected $types = ['shib'];

    /**
     * ShibLoginForm constructor.
     * @param $name
     * @param AuthenticationOptionsInterface $options
     */
    public function __construct($name, AuthenticationOptionsInterface $options)
    {
        parent::__construct($name, $options);

        $this->remove('identity');
        $this->remove('credential');

        $this->get('submit')->setLabel("Fédération d'identité");
    }
}
