<?php

namespace UnicaenAuthentification\Provider\Identity;

use Interop\Container\ContainerInterface;
use UnicaenApp\Options\ModuleOptions;
use UnicaenUtilisateur\Service\Role\RoleService;
use Zend\Ldap\Ldap;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Db identity provider factory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class DbServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $user             = $container->get('zfcuser_user_service');
        $identityProvider = new Db($user->getAuthService());
        $identityProvider->setHostLocalization($container->get('HostLocalization'));

        $unicaenAppOptions = $container->get('unicaen-app_module_options');
        /* @var $unicaenAppOptions ModuleOptions */

        $ldap = new Ldap($unicaenAppOptions->getLdap()['connection']['default']['params']);
        $identityProvider->setLdap($ldap);

        $identityProvider->setRoleService($container->get(RoleService::class));

        $config            = $container->get('BjyAuthorize\Config');
        $identityProvider->setDefaultRole($config['default_role']);
        $identityProvider->setAuthenticatedRole($config['authenticated_role']);

        return $identityProvider;
    }
}