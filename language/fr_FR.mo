��          �   %   �      `     a     ~     �     �     �  (   �                     &     -     5     E     N     ^     q     z     �     �     �     �     �     �  &   �  8         9  �  W          6     M     S     p  &   �     �     �     �     �     �     �          #     @     S  '   e     �     �     �     �     �     �  &   �  8        Q                                    
                                                                    	                    Affectations administratives Affectations recherche Aucun Aucune affectation trouvée. Aucune information disponible. Authentication failed. Please try again. Display Name Email Erreur inattendue. Hello, Inconnu Not registered? Password Password Verify Profil utilisateur Register Registration is disabled Responsabilités S'enregistrer Se connecter Sign In Sign up! Username Utilisateur connecté à l'application Vous n'êtes pas autorisé(e) à accéder à cette page. Vous n'êtes pas connecté(e) Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-27 13:41+0100
PO-Revision-Date: 2019-01-25 16:32+0100
Last-Translator: Université de Caen Basse Normandie
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate;setLabel;_
X-Poedit-Basepath: .
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: ..
 Affectations administratives Affectations recherche Aucun Aucune affectation trouvée. Aucune information disponible. Identifiant ou mot de passe incorrect. Votre nom complet Votre adresse mail Erreur inattendue. Bonjour, Inconnu Pas encore de compte ? Mot de passe Confirmez votre mot de passe Profil utilisateur Créer mon compte La création de compte est désactivée Responsabilités S'enregistrer Se connecter Se connecter Enregistrez-vous! Identifiant de connexion Utilisateur connecté à l'application Vous n'êtes pas autorisé(e) à accéder à cette page. Vous n'êtes pas connecté(e) 